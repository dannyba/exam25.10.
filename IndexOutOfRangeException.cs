﻿using System;
using System.Runtime.Serialization;

namespace exem
{
    partial class MyUniqueList
    {
        internal class IndexOutOfRangeException : Exception
        {
            public IndexOutOfRangeException()
            {
            }

            public IndexOutOfRangeException(string message) : base(message)
            {
            }

            public IndexOutOfRangeException(string message, Exception innerException) : base(message, innerException)
            {
            }

            protected IndexOutOfRangeException(SerializationInfo info, StreamingContext context) : base(info, context)
            {
            }
        }

    }
}
