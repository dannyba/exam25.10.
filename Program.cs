﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace WordGame
{
    partial class Program
    {
       
        static void Main(string[] args)
        {
            const string WELLCOM = "Welcome to the game of words,";
            const string WELLCOM2 = "In this game you will get the number of characters that contain a word.";
            const string WELLCOM3 = "What you have to do is guess a letter contained within the hidden word until you discover all its letters.";
            const string CURRECTLETTER = "------Well done for guessing a letter out of the word, keep guessing------:";
            const string WORNGLETTER = "------The Letter not found on the hide word, keep guessing------:";


            Random random_generator = new Random();
            var names = new List<string> { "dog", "cat", "danny", "hous", "room" };
            int index = random_generator.Next(names.Count);
            string result = names[index];
            char[] arr;



            arr = result.ToCharArray();
            
          
            var hideword = new string[arr.Length, 1];
            var test = new string[arr.Length, 1];
            int count_guesses = 0;


            int RandomNumber()
            {
                index = random_generator.Next(names.Count);
                return index;

            }


            void Set_Hide_Word()
            {
                for (int i = 0; i < hideword.Length; i++)
                {
                    hideword[i,0] = "_";
                }

            }

            void printhideword()
            {
                for (int i = 0; i < hideword.Length; i++)
                {
                    Console.Write(hideword[i,0]);
                }

            }


            void Set_Test()
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    test[i, 0] = arr[i].ToString();
                }

            }

            string getstring(string [,] str )
            {
                var res = String.Empty;
                for (int i = 0; i < str.Length; i++)
                {
                    res += str[i,0].ToString();
                }
                return res;
            }

            void print(string str)
            {
                Console.WriteLine(str);
            }

            void game()
            {
                var SecretWord = getstring(test);
                var SecretLine = getstring(hideword);
                do
                {
                    Console.WriteLine();
                    string word = Console.ReadLine();
                        
                        count_guesses++;
                        Console.WriteLine($"number of your guesses: {count_guesses}");


                 if (SecretWord.Contains(word))
                    {
                        for (int i = 0; i < test.Length; i++)
                            if (!String.IsNullOrEmpty(word) && word.Contains(test[i, 0]))
                        {

                            SecretLine = SecretLine.Remove(i, 1);
                            SecretLine = SecretLine.Insert(i, word);


                        }
                        print(SecretLine);
                        print(CURRECTLETTER);
                        Console.WriteLine();

                    }
                 else
                    {
                        Console.WriteLine(WORNGLETTER);

                    }
                    
                }

                while (SecretLine.Contains("_"));
                if (!SecretLine.Contains("_"))
                {
                    count_guesses = 0;
                    print($"Congratulations for guessing the word:------{SecretLine}------");
                    print("did want play again? yes/no:");
                    Ask_To_Play_Again();
                }

         
            }

            void Start_Again()
            {
                result = names[index];
                arr = result.ToCharArray();
                hideword = new string[arr.Length, 1];
                test = new string[arr.Length, 1];
                Set_Hide_Word();
                Console.WriteLine(WELLCOM);
                Console.WriteLine(WELLCOM2);
                Console.WriteLine(WELLCOM3);
                Console.WriteLine("**********************************************************************************");
                Console.WriteLine("**********************************************************************************");
                Set_Test();
              //  foreach (string d in test) // if u want to see the word
              //  Console.WriteLine(d);
                printhideword();
                Console.WriteLine();
                Console.WriteLine("guess:");
                game();

            }


            void Ask_To_Play_Again()
            {
 
                string resultasktoplayagain = Console.ReadLine();
                while (resultasktoplayagain != "no" && resultasktoplayagain != "yes")
                {
                    print("did want play again? yes/no:");
                    resultasktoplayagain = Console.ReadLine();
                }
                if(resultasktoplayagain == "no" || resultasktoplayagain == "yes")
                {
                    switch (resultasktoplayagain)
                    {
                        case "yes":
                            RandomNumber();
                            Start_Again();
                            break;
                        case "no":
                            Console.WriteLine("Thank you for participating in the game");
                            break;

                    }

                }

            }


            Console.WriteLine(WELLCOM);
            Console.WriteLine(WELLCOM2);
            Console.WriteLine(WELLCOM3);
            Console.WriteLine("************************************************************************");
            Console.WriteLine("************************************************************************");
            Set_Hide_Word();
            Set_Test();
            //foreach (string d in test) // if u want to see the word
            //  Console.WriteLine(d);
            printhideword();
            Console.WriteLine();
            Console.WriteLine("guess:");       
            game();


        }

    }
}
